import React from "react";
import AppNavigation from "./src/Navigation/AppNavigation";

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      isReady: false
    };
  }

  render() {
    return <AppNavigation />;
  }
}
