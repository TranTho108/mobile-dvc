import React, { Component } from "react";
import { View} from "react-native";
import {Text, Button} from "react-native-elements";
import styles from "./Styles/ScanBodyStyle"
import BarcodeScanner from "react-native-barcodescanner"
import { withNavigationFocus } from "react-navigation";

class ScanBody extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      barcode: '',
      cameraType: 'back',
      text: 'Scan Barcode',
      torchMode: 'off',
      type: '',
    };
  }
  barcodeReceived(e) {
    this.setState({
      barcode: e.data,
      text: `${e.data}`,
      type: e.type,
    })
    this.props.navigation.navigate("SearchScreen",{code: e.data});
  }
  renderCamera() {
    const isFocused = this.props.navigation.isFocused();
    
    if (!isFocused) {
        return null;
    } else if (isFocused) {
        return (
          <View style={styles.container}>
          <BarcodeScanner
            onBarCodeRead={this.barcodeReceived.bind(this)}
            style={{ flex: 1 }}
            torchMode={this.state.torchMode}
            cameraType={this.state.cameraType}
          />
          </View>
        )
    }
  }

  render() {
    return (
        <View style={styles.container}>
        {this.renderCamera()}
        </View>
    );
  }
}
export default withNavigationFocus(ScanBody);
