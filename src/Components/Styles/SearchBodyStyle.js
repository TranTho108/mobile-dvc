import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container:{
    flex: 1
  },
  text: {
    fontSize: 20, 
    color: 'black'
  },
  textInput: {
    height: 40,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#fff',
    margin: 10,
  },
  picker: {
    height: 40,
  },
  pickerview: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#eaeaea',
    margin: 10,
    width: 300
  },
  qrscan: {
    margin : 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  listrow:{
    justifyContent: 'center',
    alignItems: 'flex-start',
    borderRadius: 5,
    backgroundColor: '#fff',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    flex: 1
  },
  ngay: {
    color: '#1976D2',
    fontStyle: 'italic',
    },
  ma: {
    fontSize: 16,
    color: '#0D47A1',
    fontWeight: 'bold'
  },
  chu: {
    fontSize: 12,
    color: 'black',
  },
  nguoinop: {
    fontSize: 12,
    color: 'black',
  },
  ten: {
    fontSize: 12,
    color: 'black',
  },
  trangthai: {
    fontSize: 14,
    color: '#4caf50',
  },
  place: {
    padding: 20,
  },
  container1  : {
    backgroundColor: '#fff',
    margin:10,
    overflow:'hidden',
},
  title: {
    textAlign: 'center',
    fontSize: 22,
    fontWeight: '300',
    marginBottom: 20,
  },
  header: {
    backgroundColor: '#F5FCFF',
    padding: 10,
  },
  headerText: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: '500',
  },
  content: {
    padding: 20,
    backgroundColor: '#fff',
  },
  titleContainer : {
    flexDirection: 'row'
},
title1       : {
    flex    : 1,
    padding : 10,
    color   :'#2a2f43',
    fontWeight:'bold'
},
button      : {

},
buttonImage : {
    width   : 30,
    height  : 25
},
body        : {
    padding     : 10,
    paddingTop  : 0
}
})
