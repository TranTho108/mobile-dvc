import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  text: {
    fontSize: 20, 
    color: 'black'
  },
  container:{
    backgroundColor: "#fff",
    flex: 1
  },
  listrow:{
    justifyContent: 'center',
    alignItems: 'flex-start',
    borderRadius: 5,
    backgroundColor: '#fff',
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    flex: 1
  },
  ngay: {
    color: '#1976D2',
    fontStyle: 'italic',
    },
  ma: {
    fontSize: 16,
    color: '#0D47A1',
    fontWeight: 'bold'
  },
  chu: {
    fontSize: 12,
    color: 'black',
  },
  nguoinop: {
    fontSize: 12,
    color: 'black',
  },
  ten: {
    fontSize: 12,
    color: 'black',
  },
  trangthai: {
    fontSize: 14,
    color: '#4caf50',
  },
  empty: {
    fontSize: 16,
    color: 'black',
  },
  listrow1:{
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'flex-start',
    borderTopWidth: 2,
    borderColor: '#eaeaea',
    margin: 10,
  },
  text1: {
    color: 'black',
    marginLeft: 10
  },
  danhmuc: {
    borderColor: '#e8e8e8',
    borderBottomWidth: 0.75,
    borderRightWidth: 0.75,
    borderTopWidth: 0.75,
    borderLeftWidth: 1.5,
    flex: 2.2/7, 
    minHeight: 40, 
    alignItems: 'flex-start', 
    justifyContent: 'center'
  },
  danhmucdau: {
    borderColor: '#e8e8e8',
    borderBottomWidth: 0.75,
    borderRightWidth: 0.75,
    borderLeftWidth: 1.5,
    borderTopWidth: 1.5,
    flex: 2.2/7, 
    minHeight: 40, 
    alignItems: 'flex-start', 
    justifyContent: 'center'
  },
  danhmuccuoi: {
    borderColor: '#e8e8e8',
    borderBottomWidth: 1.5,
    borderRightWidth: 0.75,
    borderTopWidth: 0.75,
    borderLeftWidth: 1.5,
    flex: 2.2/7, 
    minHeight: 40, 
    alignItems: 'flex-start', 
    justifyContent: 'center'
  },
  row: {
    flex: 1, 
    flexDirection: 'row'
  },
  giatri: {
    borderColor: '#e8e8e8',
    borderBottomWidth: 0.75,
    borderRightWidth: 1.5,
    borderTopWidth: 0.75,
    borderLeftWidth: 0.75,
    flex: 4.8/7, 
    minHeight: 40, 
    alignItems: 'flex-start', 
    justifyContent: 'center',
    paddingTop: 5,
    paddingBottom: 5
  },
  giatridau: {
    borderColor: '#e8e8e8',
    borderBottomWidth: 0.75,
    borderRightWidth: 1.5,
    borderLeftWidth: 0.75,
    borderTopWidth: 1.5,
    flex: 4.8/7, 
    minHeight: 40, 
    alignItems: 'flex-start', 
    justifyContent: 'center',
    paddingTop: 5,
    paddingBottom: 5
  },
  giatricuoi: {
    borderColor: '#e8e8e8',
    borderBottomWidth: 1.5,
    borderRightWidth: 1.5,
    borderTopWidth: 0.75,
    borderLeftWidth: 0.75,
    flex: 4.8/7, 
    minHeight: 40, 
    alignItems: 'flex-start', 
    justifyContent: 'center',
    paddingTop: 5,
    paddingBottom: 5
  },
  place: {
    padding: 20,
  },
})
