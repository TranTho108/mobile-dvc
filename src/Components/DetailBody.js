import React, { Component } from "react";
import { View, AsyncStorage, FlatList, ActivityIndicator, ScrollView, TouchableOpacity, ToastAndroid} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import axios from "axios"
import styles from "./Styles/MainBodyStyle"
import {parseString} from "react-native-xml2js"
import SearchFileByCode from "../Api/GetFileByCode"
import ScrollableTabView, { ScrollableTabBar, } from 'react-native-scrollable-tab-view';
import Timeline from 'react-native-timeline-listview'


export default class DetailBody extends Component {

  constructor(props) {
    super(props);
    this.state = {
        data: [],
        timeline: [],
        history: [],
        follow: false
      }
    }

  componentWillMount() {
    var data = [];
    data = this.props.navigation.state.params.data
    var timeline =[];
    data.QuaTrinhXuLy.map((data) => {
      timeline.push({"time": data.NgayThucHien, "title": data.NoiDung, "description": "Người gửi : "+ data.NguoiGui + "\nNgười nhận : " + data.NguoiNhan })
    });
    this.setState({data: data, timeline: timeline})
    this._getHistory(data.MaHoSo);
  }

  _getHistory = async(MaHoSo) => {
    await AsyncStorage.getItem('history')
    .then((history) => {
        const arr = history ? JSON.parse(history) : [];
        this.setState({history: arr})
        if(arr.includes(MaHoSo)){
          this.setState({follow: true})
        }
  })
    .catch((err)=> console.log(err))
  }

  _pushItem = async (item) => {
      const arr = this.state.history
      arr.push(item)
      const arr1 = [...new Set([...arr])]
      await AsyncStorage.setItem('history', JSON.stringify(arr1));
      ToastAndroid.show(`Đã thêm hồ sơ vào danh sách theo dõi`, ToastAndroid.SHORT);
      this.setState({follow: true})
  }

  _deleteItem = async (MaHoSo) => {
    var arr = this.state.history
    var i = arr.indexOf(MaHoSo);
    if (i != -1) {
      arr.splice(i,1);
    }
    await AsyncStorage.setItem('history', JSON.stringify(arr));
    ToastAndroid.show(`Đã bỏ theo dõi hồ sơ`, ToastAndroid.SHORT);
    this.setState({follow: false})
  }

  _checkItem = () => {
    if(this.state.follow){
      return(
        <TouchableOpacity style={{borderRadius: 4, backgroundColor: '#FF7043', flexDirection: 'row', width: null, margin: 20, alignItems: 'center', justifyContent: 'center', height: 30}} onPress = {() => this._deleteItem(this.state.data.MaHoSo)}>
            <Icon 
              name='delete-sweep'
              size={20}
              color= '#fff'
            />
            <Text style={{color: '#fff'}}> BỎ THEO DÕI</Text>
        </TouchableOpacity>
      )
    }
    else{
      return(
        <TouchableOpacity style={{borderRadius: 4, backgroundColor: '#03A9F4', flexDirection: 'row', width: null, margin: 20, alignItems: 'center', justifyContent: 'center', height: 30}} onPress = {() => this._pushItem(this.state.data.MaHoSo)}>
            <Icon 
              name='near-me'
              size={20}
              color= '#fff'
            />
            <Text style={{color: '#fff'}}>  THEO DÕI</Text>
        </TouchableOpacity>
      )
    }
  }

  render() {
    const data = this.state.data
    return (
      <View style={styles.container}>
      <ScrollableTabView
            tabBarBackgroundColor={'#4caf50'}
            tabBarActiveTextColor={'#fff'}
            tabBarInactiveTextColor={'#e8e8e8'}
            tabBarUnderlineStyle={{backgroundColor: 'transparent'}}
            style={{}}
            initialPage={0}
            renderTabBar={() => <ScrollableTabBar />}
          >
          <ScrollView tabLabel='THÔNG TIN CƠ BẢN' style={{backgroundColor: '#fff', flex: 1}}>
            <View style={{margin: 15}}>
              <Text style={{color: 'black', fontSize: 16}}>Mã Hồ Sơ :</Text>
              <View style={{padding: 10, borderBottomWidth: 0.3}}><Text style={{color: '#757575'}}>{data.MaHoSo}</Text></View>
            </View>
            <View style={{margin: 15}}>
              <Text style={{color: 'black', fontSize: 16}}>Chủ Hồ Sơ :</Text>
              <View style={{padding: 10, borderBottomWidth: 0.3}}><Text style={{color: '#757575'}}>{data.ChuHoSo}</Text></View>
            </View>
            <View style={{margin: 15}}>
              <Text style={{color: 'black', fontSize: 16}}>Người Nộp :</Text>
              <View style={{padding: 10, borderBottomWidth: 0.3}}><Text style={{color: '#757575'}}>{data.NguoiNop}</Text></View>
            </View>
            <View style={{margin: 15}}>
              <Text style={{color: 'black', fontSize: 16}}>Tên Hồ Sơ :</Text>
              <View style={{padding: 10, borderBottomWidth: 0.3}}><Text style={{color: '#757575'}}>{data.TenHoSo}</Text></View>
            </View>
            <View style={{margin: 15}}>
              <Text style={{color: 'black', fontSize: 16}}>Ngày Tiếp Nhận :</Text>
              <View style={{padding: 10, borderBottomWidth: 0.3}}><Text style={{color: '#757575'}}>{data.NgayTiepNhan}</Text></View>
            </View>
            <View style={{margin: 15}}>
              <Text style={{color: 'black', fontSize: 16}}>Ngày Hẹn Trả :</Text>
              <View style={{padding: 10, borderBottomWidth: 0.3}}><Text style={{color: '#757575'}}>{data.NgayHenTra}</Text></View>
            </View>
            <View style={{margin: 15}}>
              <Text style={{color: 'black', fontSize: 16}}>Ngày Trả Kết Quả :</Text>
              <View style={{padding: 10, borderBottomWidth: 0.3}}><Text style={{color: '#757575'}}>{data.NgayTraKetQua}</Text></View>
            </View>
            <View style={{margin: 15}}>
              <Text style={{color: 'black', fontSize: 16}}>Ngày Trả Công Dân :</Text>
              <View style={{padding: 10, borderBottomWidth: 0.3}}><Text style={{color: '#757575'}}>{data.NgayTraCongDan}</Text></View>
            </View>
            <View style={{margin: 15}}>
              <Text style={{color: 'black', fontSize: 16}}>Trạng Thái :</Text>
              <View style={{padding: 10, borderBottomWidth: 0.3}}><Text style={{color: '#757575'}}>{data.TrangThaiMobile}</Text></View>
            </View>
            <View style={{margin: 15}}>
              <Text style={{color: 'black', fontSize: 16}}>Đơn Vị :</Text>
              <View style={{padding: 10, borderBottomWidth: 0.3}}><Text style={{color: '#757575'}}>{data.DonVi}</Text></View>
            </View>
            <View style={{margin: 15}}>
              <Text style={{color: 'black', fontSize: 16}}>Mã Đơn Vị :</Text>
              <View style={{padding: 10, borderBottomWidth: 0.3}}><Text style={{color: '#757575'}}>{data.MaDonVi}</Text></View>
            </View>
            {this._checkItem()}
          </ScrollView>
            <View tabLabel='QUÁ TRÌNH XỬ LÝ'>
            <ScrollView style={{}}>
            <Timeline
              circleSize={20}
              circleColor='rgb(45,156,219)'
              lineColor='rgb(45,156,219)'
              timeContainerStyle={{minWidth:50,}}
              timeStyle={{textAlign: 'center', backgroundColor:'rgb(45,156,219)', color:'white', borderRadius:7, padding: 2}}
              descriptionStyle={{color:'gray'}}
              titleStyle={{color: 'black'}}
              data={this.state.timeline}
              innerCircle={'dot'}
              
            />
            </ScrollView>
            </View>
          </ScrollableTabView>
      </View>
    );
    }
}
