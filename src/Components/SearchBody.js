import React, { Component } from "react";
import { View, TextInput, Picker, TouchableOpacity, Image, FlatList, ScrollView, ToastAndroid, AsyncStorage, ActivityIndicator,Modal,Animated} from "react-native";
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import {Text, Button, Icon} from "react-native-elements";
import styles from "./Styles/SearchBodyStyle"
import axios from "axios"
import Placeholder from 'rn-placeholder';
import {parseString} from "react-native-xml2js"

const arr =  [
  {
    "url": 'https://dvc1.mard.gov.vn',
    "name": "Bộ Nông nghiệp & Phát triển nông thôn",
  }
]
const arr1 =  [
  {
    "url": 'http://dichvucong.hatinh.gov.vn',
    "name": " Trung tâm HCC Tỉnh Hà Tĩnh",
  },
  {
    "url": 'http://dichvucong.hatinhcity.gov.vn',
    "name": " Trung tâm HCC Thành phố Hà Tĩnh & Các xã",
  },
  {
    "url": 'http://dvchuongson.hatinh.gov.vn',
    "name": " Trung tâm HCC Huyện Hương Sơn & Các xã",
  },
  {
    "url": 'http://dvcnghixuan.hatinh.gov.vn',
    "name": "Trung tâm HCC Huyện Nghi Xuân & Các xã",
  },
  {
    "url": 'http://dvcductho.hatinh.gov.vn',
    "name": "Trung tâm HCC Huyện Đức Thọ & Các xã",
  },
  {
    "url": 'http://dvchonglinh.hatinh.gov.vn',
    "name": "Trung tâm HCC Thị xã Hồng Lĩnh & Các xã",
  },
  {
    "url": 'http://dvchuongkhe.hatinh.gov.vn',
    "name": " Trung tâm HCC Huyện Hương Khê & Các xã",
  },
  {
    "url": 'http://dvcthachha.hatinh.gov.vn',
    "name": "Trung tâm HCC Huyện Thạch Hà & Các xã",
  },
  {
    "url": 'http://dvctxkyanh.hatinh.gov.vn',
    "name": " Trung tâm HCC Thị xã Kỳ Anh & Các xã",
  },
  {
    "url": 'http://dvclocha.hatinh.gov.vn',
    "name": "Trung tâm HCC Huyện Lộc Hà & Các xã",
  },
  {
    "url": 'http://dvcvuquang.hatinh.gov.vn',
    "name": "Trung tâm HCC Huyện Vũ Quang & Các xã",
  },
  {
    "url": 'http://dvccamxuyen.hatinh.gov.vn',
    "name": "Trung tâm HCC Huyện Cẩm Xuyên & Các xã",
  },
  {
    "url": 'http://dvckyanh.hatinh.gov.vn',
    "name": "Trung tâm HCC Huyện Kỳ Anh & Các xã",
  },
  {
    "url": 'http://dvccanloc.hatinh.gov.vn',
    "name": "Trung tâm HCC Huyện Can Lộc & Các xã",
  }
]
const arr2 =  [
  {
    "url": 'http://nshn.com.vn',
    "name": "Công ty TNHH Một thành viên Nước sạch Hà Nội",
  },
  {
    "url": 'http://dvc.nuocsachso3hn.vn',
    "name": "Công ty Cổ phần sản xuất Kinh doanh Nước sạch số 3 Hà Nội",
  }
]

export default class SearchBody extends Component {
  constructor(props) {
  super(props);

  this.icons = {
    'up'    : require('../Images/Arrowhead-01-128.png'),
    'down'  : require('../Images/Arrowhead-Down-01-128.png')
  };

	this.state = {
      donvi: '',
      code: 'CTYQM000010',
      empty: false,
      data: [],
      count: null,
      loading: false,
      url: '',
      modalVisible: false,
      expanded    : false,
      animation   : new Animated.Value(),
      expanded1    : false,
      animation1   : new Animated.Value(),
      expanded2    : false,
      animation2   : new Animated.Value(),
      group1: null,
      group2: null,
      group3: null,
      arr: arr,
      arr1: arr1,
      arr2: arr2,
      history: []
    }
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  toggle(){
    let initialValue    = this.state.expanded? this.state.maxHeight + this.state.minHeight : this.state.minHeight,
        finalValue      = this.state.expanded? this.state.minHeight : this.state.maxHeight + this.state.minHeight;

    this.setState({
        expanded : !this.state.expanded
    });

    this.state.animation.setValue(initialValue);
    Animated.spring(
        this.state.animation,
        {
            toValue: finalValue
        }
    ).start();
}

_setMaxHeight(event) {
  if (!this.state.maxHeight) {
    this.setState({
      maxHeight: event.nativeEvent.layout.height,
    });
  }
}

_setMinHeight(event) {
  if (!this.state.minHeight) {
    this.setState({
      minHeight: event.nativeEvent.layout.height,
      animation: new Animated.Value(event.nativeEvent.layout.height),
    });
  }
}

toggle1(){
  let initialValue   = this.state.expanded1? this.state.maxHeight1 + this.state.minHeight1 : this.state.minHeight1,
      finalValue      = this.state.expanded1? this.state.minHeight1 : this.state.maxHeight1 + this.state.minHeight1;

  this.setState({
      expanded1 : !this.state.expanded1
  });

  this.state.animation1.setValue(initialValue);
  Animated.spring(
      this.state.animation1,
      {
          toValue: finalValue
      }
  ).start();
}

_setMaxHeight1(event) {
if (!this.state.maxHeight1) {
  this.setState({
    maxHeight1: event.nativeEvent.layout.height,
  });
}
}

_setMinHeight1(event) {
if (!this.state.minHeight1) {
  this.setState({
    minHeight1: event.nativeEvent.layout.height,
    animation1: new Animated.Value(event.nativeEvent.layout.height),
  });
}
}

toggle2(){
  let initialValue   = this.state.expanded2? this.state.maxHeight2 + this.state.minHeight2 : this.state.minHeight2,
      finalValue      = this.state.expanded2? this.state.minHeight2 : this.state.maxHeight2 + this.state.minHeight2;

  this.setState({
      expanded2 : !this.state.expanded2
  });

  this.state.animation2.setValue(initialValue);
  Animated.spring(
      this.state.animation2,
      {
          toValue: finalValue
      }
  ).start();
}

_setMaxHeight2(event) {
if (!this.state.maxHeight2) {
  this.setState({
    maxHeight2: event.nativeEvent.layout.height,
  });
}
}

_setMinHeight2(event) {
if (!this.state.minHeight2) {
  this.setState({
    minHeight2: event.nativeEvent.layout.height,
    animation2: new Animated.Value(event.nativeEvent.layout.height),
  });
}
}

_setUrl = async (url, name) => {
  await AsyncStorage.setItem('url', url);
  await AsyncStorage.setItem('name', name);
}

  componentDidMount(){
    this.subs = [
    this.props.navigation.addListener('didFocus', () => {
      this._getUrl()
      this._getHistory()
      if(this.props.navigation.state.params){
      this.setState({code : this.props.navigation.state.params.code})
    }})]
  }

  componentWillUnmount() {
    this.subs.forEach((sub) => {
      sub.remove();
    });
  }

  _getUrl = async() =>  {
    var url = await AsyncStorage.getItem('url')
    var name = await AsyncStorage.getItem('name')
    this.setState({donvi: name, url: url})
  }

  SearchFile = async() => {
      if(this.state.code === ''){
        ToastAndroid.show(`Nhập mã hồ sơ`, ToastAndroid.SHORT)
      }
      else{
      this.setState({data: [],loading: true})
      const xmlReqBody = `<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">   <soap:Body>     <ServiceLayThongTinHoSo xmlns="http://schemas.microsoft.com/sharepoint/soap/">       <tukhoa>`+ this.state.code +`</tukhoa>     </ServiceLayThongTinHoSo>   </soap:Body> </soap:Envelope>`
      await axios('http://113.160.53.62:9009/_layouts/tandan/dvc/DVCService.asmx?wsdl', {
        method: 'POST',
        headers: {
          "Content-Type": 'text/xml; charset="utf-8"'
        },
        data: xmlReqBody,
        dataType: "xml",
      })
      .then(function (response) {
        var xml = response.data
        parseString(xml, function (err, result) {
          var t = result['soap:Envelope']
          var t1 = t['soap:Body']
          var t2 = t1[0].ServiceLayThongTinHoSoResponse[0].ServiceLayThongTinHoSoResult[0]
          xml = t2
        });
        return xml;
      })
      .then((data) => {
        var data1 = JSON.parse(data)
        this.setState({data: data1.DSHoSo,empty: true})
        this.setState({count: this.state.data.length, loading: false})
        if(this.state.count === 0){
          ToastAndroid.show(`Không tìm thấy hồ sơ`, ToastAndroid.SHORT);}
        else{
          ToastAndroid.show(`Đã tìm thấy `+ this.state.count +` hồ sơ. Nhấn vào hồ sơ để xem chi tiết`, ToastAndroid.SHORT);}
      }
    )
      .catch((err) => console.log(err))
  }}

  PushItem = async (item) => {
    await AsyncStorage.getItem('history')
    .then((history) => {
        const arr = history ? JSON.parse(history) : [];
        arr.push(item)
        const arr1 = [...new Set([...arr])]
        this.setState({history: arr1})
      AsyncStorage.setItem('history', JSON.stringify(arr1));
      ToastAndroid.show(`Đã thêm hồ sơ vào danh sách theo dõi`, ToastAndroid.SHORT);
  })
    .catch((err)=> console.log(err))
  }

  _getHistory = async() => {
    await AsyncStorage.getItem('history')
    .then((history) => {
        const arr = history ? JSON.parse(history) : [];
        this.setState({history: arr})
  })
    .catch((err)=> console.log(err))
  }

  _checkItem = (MaHoSo) => {
    if(this.state.history.includes(MaHoSo)){
      return(
        <TouchableOpacity style={{borderRadius: 4, backgroundColor: '#FF7043', flexDirection: 'row', padding: 5}} onPress = {() => this._deleteItem(MaHoSo)}>
            <Icon 
              name='delete-sweep'
              size={20}
              color= '#fff'
            />
            <Text style={{color: '#fff'}}> BỎ THEO DÕI</Text>
        </TouchableOpacity>
      )
    }
    else{
      return(
        <TouchableOpacity style={{borderRadius: 4, backgroundColor: '#03A9F4', flexDirection: 'row', padding: 5}} onPress = {() => this.PushItem(MaHoSo)}>
            <Icon 
              name='near-me'
              size={20}
              color= '#fff'
            />
            <Text style={{color: '#fff'}}>  THEO DÕI</Text>
        </TouchableOpacity>
      )
    }
  }

  _deleteItem = async (MaHoSo) => {
    var arr = this.state.history
    var i = arr.indexOf(MaHoSo);
    if (i != -1) {
      arr.splice(i,1);
    }
    await AsyncStorage.setItem('history', JSON.stringify(arr));
    ToastAndroid.show(`Đã bỏ theo dõi hồ sơ`, ToastAndroid.SHORT);
    this.setState({history: arr})
  }
 
  showLoading = () => {
    if(this.state.loading === true)
        return ( <View>
          <View style={styles.place}>
        <Placeholder.ImageContent
          size={60}
          animate="fade"
          lineNumber={6}
          lineSpacing={5}
          lastLineWidth="30%"
          onReady={!this.state.loading}
          color= '#fff'
        >
        </Placeholder.ImageContent></View>
        <View style={styles.place}>
        <Placeholder.ImageContent
          size={60}
          animate="fade"
          lineNumber={6}
          lineSpacing={5}
          lastLineWidth="30%"
          onReady={!this.state.loading}
          color= '#fff'
        >
        </Placeholder.ImageContent></View>
        <View style={styles.place}>
        <Placeholder.ImageContent
          size={60}
          animate="fade"
          lineNumber={6}
          lineSpacing={5}
          lastLineWidth="30%"
          onReady={!this.state.loading}
          color= '#fff'
        >
        </Placeholder.ImageContent></View>
        <View style={styles.place}>
        <Placeholder.ImageContent
          size={60}
          animate="fade"
          lineNumber={6}
          lineSpacing={5}
          lastLineWidth="30%"
          onReady={!this.state.loading}
          color= '#fff'
        >
        </Placeholder.ImageContent></View>
        <View style={styles.place}>
        <Placeholder.ImageContent
          size={60}
          animate="fade"
          lineNumber={6}
          lineSpacing={5}
          lastLineWidth="30%"
          onReady={!this.state.loading}
          color= '#fff'
        >
        </Placeholder.ImageContent></View>
        </View>)
  }

  _renderItem = ({item}) => {
    return (
    <TouchableOpacity onPress = {() => {this.props.navigation.navigate("DetailScreen", {data: item})}}>
      <View style={styles.listrow} >
      <View style={{padding: 15, alignSelf: 'stretch',}}>
        <View style= {{flexDirection: "row", justifyContent: 'space-between'}}><View style= {{}}><Text style={styles.ma}>{item.MaHoSo}</Text></View><View style= {{}}><Text style={styles.ngay}>{item.NgayTiepNhan}</Text></View></View>
        <View style= {{padding: 5}}>
        <Text style={styles.chu}>Chủ hồ sơ : {item.ChuHoSo}</Text>
        <Text style={styles.ten}>Tên hồ sơ : {item.TenHoSo}</Text>
        </View>
        <View style= {{justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
        {this._checkItem(item.MaHoSo)}
        <Text style={styles.trangthai}>{item.TrangThaiMobile}  </Text></View>
        </View>
      </View>
    </TouchableOpacity>
    )}

  render() {

    let icon1 = this.icons['down'];
    let icon = this.icons['down'];
    let icon2 = this.icons['down'];

    if(this.state.expanded){
        icon = this.icons['up'];
    }

    if(this.state.expanded1){
      icon1 = this.icons['up'];
  }
  if(this.state.expanded2){
    icon2 = this.icons['up'];
  }
 
    return (
      <ScrollView style={{flex: 1, backgroundColor: '#EEEEEE'}}>
        <View style={styles.container}>
        <View style={{margin: 15}}>
              <Text style={{color: 'black', fontSize: 16}}>Đơn Vị :</Text>
              <View style={{padding: 10, borderBottomWidth: 0.3, flexDirection: 'row', justifyContent: 'space-between'}}>
                <Text style={{color: '#757575', maxWidth: 250}}>{this.state.donvi}</Text>
                <Icon
                  name="edit"
                  color="black"
                  onPress={() => {this.setModalVisible(true)}}
                />
                <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
          }}
          >
      <View style={{flex: 1,
          flexDirection: 'column',
          backgroundColor: '#fff',
          marginTop: 100,
          borderTopLeftRadius: 10,
          borderTopRightRadius: 10}}>
        <View style={{marginTop: 10, marginBottom: 10}}><Text style={{textAlign: 'center', color: 'black', fontSize:18}}>Chọn Đơn Vị</Text></View>
        <ScrollView>
        <Animated.View 
            style={[styles.container1,{height: this.state.animation}]}>
            <TouchableOpacity style={styles.titleContainer} onLayout={this._setMinHeight.bind(this)}
                    onPress={this.toggle.bind(this)}
                    underlayColor="#f1f1f1">
                    <Text style={styles.title1}>Cấp Bộ</Text>
                    <Image
                        style={styles.buttonImage}
                        source={icon}
                    ></Image>
                </TouchableOpacity>
            
            <View style={styles.body} onLayout={this._setMaxHeight.bind(this)}>
            <RadioGroup 
              size={24}
              thickness={2}
              color='black'
              highlightColor='#ccc8b9'
              selectedIndex={this.state.group1}
              onSelect = {(index, value) => {this.setState({group2: null,group1: index,group3: null}), this._setUrl(arr[0].url, arr[0].name)}}>

              <RadioButton value={'bnn'} >
                <Text> Bộ Nông nghiệp & Phát triển nông thôn</Text>
              </RadioButton>

            </RadioGroup>
            </View>

        </Animated.View>

        <Animated.View 
            style={[styles.container1,{height: this.state.animation1}]}>
            <TouchableOpacity style={styles.titleContainer} onLayout={this._setMinHeight1.bind(this)}
                    onPress={this.toggle1.bind(this)}
                    underlayColor="#f1f1f1">
                    <Text style={styles.title1}>Tỉnh Hà Tĩnh</Text>
                    <Image
                        style={styles.buttonImage}
                        source={icon1}
                    ></Image>
                </TouchableOpacity>
            
            <View style={styles.body} onLayout={this._setMaxHeight1.bind(this)}>
              <RadioGroup 
              size={24}
              thickness={2}
              color='black'
              highlightColor='#ccc8b9'
              selectedIndex={this.state.group2}
              onSelect = {(index, value) => {this.setState({group1: null,group2: index,group3: null}), this._setUrl(arr1[index].url, arr1[index].name)}}>

              <RadioButton value={'ht1'} >
                <Text> Trung tâm HCC Tỉnh Hà Tĩnh</Text>
              </RadioButton>

              <RadioButton value={'ht2'}>
                <Text> Trung tâm HCC Thành phố Hà Tĩnh & Các xã</Text>
              </RadioButton>

              <RadioButton value={'ht3'}>
                <Text> Trung tâm HCC Huyện Hương Sơn & Các xã</Text>
              </RadioButton>

              <RadioButton value={'ht4'}>
                <Text> Trung tâm HCC Huyện Nghi Xuân & Các xã</Text>
              </RadioButton>

              <RadioButton value={'ht5'}>
                <Text> Trung tâm HCC Huyện Đức Thọ & Các xã</Text>
              </RadioButton>

              <RadioButton value={'ht6'}>
                <Text> Trung tâm HCC Thị xã Hồng Lĩnh & Các xã</Text>
              </RadioButton>

              <RadioButton value={'ht7'}>
                <Text> Trung tâm HCC Huyện Hương Khê & Các xã</Text>
              </RadioButton>

              <RadioButton value={'ht8'}>
                <Text> Trung tâm HCC Huyện Thạch Hà & Các xã</Text>
              </RadioButton>
              
              <RadioButton value={'ht9'}>
                <Text> Trung tâm HCC Thị xã Kỳ Anh & Các xã</Text>
              </RadioButton>

              <RadioButton value={'ht10'}>
                <Text> Trung tâm HCC Huyện Lộc Hà & Các xã</Text>
              </RadioButton>

              <RadioButton value={'ht11'}>
                <Text> Trung tâm HCC Huyện Vũ Quang & Các xã</Text>
              </RadioButton>

              <RadioButton value={'ht12'}>
                <Text> Trung tâm HCC Huyện Cẩm Xuyên & Các xã</Text>
              </RadioButton>

              <RadioButton value={'ht13'}>
                <Text> Trung tâm HCC Huyện Kỳ Anh & Các xã</Text>
              </RadioButton>

              <RadioButton value={'ht14'}>
                <Text> Trung tâm HCC Huyện Can Lộc & Các xã</Text>
              </RadioButton>

              </RadioGroup>
            </View>

        </Animated.View>

        <Animated.View 
            style={[styles.container1,{height: this.state.animation2}]}>
                <TouchableOpacity 
                    style={styles.button} 
                    onPress={this.toggle2.bind(this)}
                    underlayColor="#f1f1f1"
                    style={styles.titleContainer} onLayout={this._setMinHeight2.bind(this)}>
                    <Text style={styles.title1}>Các đơn vị khác</Text>
                    <Image
                        style={styles.buttonImage}
                        source={icon2}
                    ></Image>
                </TouchableOpacity>
            
            <View style={styles.body} onLayout={this._setMaxHeight2.bind(this)}>
            <RadioGroup 
              size={24}
              thickness={2}
              color='black'
              highlightColor='#ccc8b9'
              selectedIndex={this.state.group3}
              onSelect = {(index, value) => {this.setState({group2: null,group3: index,group1: null}), this._setUrl(arr2[index].url, arr2[index].name)}}>

              <RadioButton value={'nshn'} >
                <Text> Công ty TNHH Một thành viên Nước sạch Hà Nội</Text>
              </RadioButton>

              <RadioButton value={'ns3hn'} >
                <Text> Công ty Cổ phần sản xuất Kinh doanh Nước sạch số 3 Hà Nội</Text>
              </RadioButton>

            </RadioGroup>
            </View>

        </Animated.View>
        </ScrollView>
        </View>
      <View style={{flexDirection: 'row', justifyContent: 'flex-end', backgroundColor: '#fff', paddingBottom: 10}}>
      <Button
                title='Thoát'
                buttonStyle={{
                backgroundColor: "#fff",
                height: 45,
                borderColor: "transparent",
                borderWidth: 0,
                borderRadius: 20,
                alignSelf: 'center'
              }}
              containerStyle={{ marginTop: 20 }}
              textStyle={{color: 'black'}}
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
              }}
              />
              <Button
                title='Tiếp tục'
                buttonStyle={{
                backgroundColor: "#fff",
                height: 45,
                borderColor: "transparent",
                borderWidth: 0,
                borderRadius: 20,
                alignSelf: 'center'
              }}
              containerStyle={{ marginTop: 20 }}
              textStyle={{color: 'black'}}
              onPress={() => {this.setModalVisible(!this.state.modalVisible), this._getUrl()}}
              />
              </View>
        </Modal>
              </View>
        </View>
        
        <TextInput
            style={styles.textInput}
            placeholder="Nhập mã hồ sơ"
            onChangeText={(code) => this.setState({code: code})}
            value={this.state.code}
            underlineColorAndroid={'transparent'}
        />
        <View style={styles.qrscan}><Text>Hoặc quét mã BR</Text>
        <TouchableOpacity style={{height:40, width:50,borderRadius: 2,borderColor: "#e8e8e8",borderWidth: 0.5,marginLeft: 10}} onPress={() => this.props.navigation.navigate("ScanScreen")} >  
            <Image 
               source={{uri: "https://static.thenounproject.com/png/74445-200.png"}} 
               style={{height:40, width:50}} />
          </TouchableOpacity>
        </View>
        <Button
            title='TÌM KIẾM'
            buttonStyle={{
                backgroundColor: "#4caf50",
                height: 45,
                borderColor: "transparent",
                borderWidth: 0,
                borderRadius: 20
              }}
              containerStyle={{ marginTop: 20 }}
              onPress ={ () => this.SearchFile()}
        />
        {this.showLoading()}
          <FlatList
                data={this.state.data}
                renderItem={this._renderItem}
                keyExtractor={(item, index) => index.toString()}
                //ListEmptyComponent={this.showEmptyListView()}
                extraData={this.state}
        />
        </View>
      </ScrollView>
    );
  }
}
