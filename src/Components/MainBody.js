import React, { Component } from "react";
import { View, AsyncStorage, FlatList, ActivityIndicator, TouchableOpacity, TextInput} from "react-native";
import {Text, Button, Icon} from "react-native-elements";
import axios from "axios"
import styles from "./Styles/MainBodyStyle"
import {parseString} from "react-native-xml2js"
import SearchFileByCode from "../Api/GetFileByCode"
import Placeholder from 'rn-placeholder';
import FCM, { NotificationActionType } from "react-native-fcm";
import ActionButton from 'react-native-action-button';


export default class MainBody extends Component {

  constructor(props) {
    super(props);
    this.state = {
        history: [],
        data: [],
        loading: true,
        data1: [],
        token: ''
      }
    }

  async componentWillMount() {
      this.setState({ data: [], data1: []}),
      FCM.requestPermissions()
      FCM.getFCMToken().then(token => {
        console.log("TOKEN (getFCMToken)", token);
        this.setState({ token: token || "" });
      });
      AsyncStorage.getItem('history')
      .then(async(history) => {
        const arr = await history ? JSON.parse(history) : [];
        this.setState({history: arr})
        if(this.state.history.length === 0 ){this.setState({loading: false})}
        arr.forEach(async(item) => {
          file = await SearchFileByCode(item)
          this.setState({data: this.state.data.concat(file), loading: false, data1: this.state.data.concat(file)})
        })
      })
  }
  componentDidMount(){
    this.subs = [
      this.props.navigation.addListener('didFocus',() => {
        AsyncStorage.getItem('history')
        .then(async(history) => {
          const arrnew = await history ? JSON.parse(history) : []
          if(arrnew.length > 0 ){
          if(this.state.history.length !== arrnew.length && arrnew.length > 0 ){
            this.setState({data: [], data1: [], history: arrnew})
            arrnew.forEach(async(item) => {
              file = await SearchFileByCode(item)
              this.setState({data: this.state.data.concat(file), loading: false, data1: this.state.data.concat(file)})
            })
          }}
          else {
            this.setState({data: [], loading: false})
          }
        })
      }),
    ];
  }

  componentWillUnmount() {
    
  }

  showEmptyListView = () => {
    return (
      <View style={{alignItems: 'center', justifyContent: 'center', marginTop: 20}}><Text style={styles.empty}>Không có hồ sơ được theo dõi</Text></View>
    );
  }

  deleteItem = async (MaHoSo) => {
    var data = this.state.data.filter(item => {
      const MaHoSo1 = item.MaHoSo
      return MaHoSo1 !== MaHoSo
    })
    var arr = this.state.history
    var i = arr.indexOf(MaHoSo);
    if (i != -1) {
      arr.splice(i,1);
    }
    await AsyncStorage.setItem('history', JSON.stringify(arr));
    this.setState({data: data})
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.timkiem !== null){
      const newData1 = this.state.data.filter(item =>{
        const groupnamedata = item.MaHoSo
        const codedata = item.TenHoSo
        return groupnamedata.indexOf(nextProps.timkiem) > -1 || codedata.indexOf(nextProps.timkiem) > -1
    })
      this.setState({
        data1: newData1
      })}
  }

  _renderItem = ({item}) => {
    return (
      <View style={styles.listrow}>
      <View style={{alignSelf: 'flex-end', paddingRight: 5}}>
      <Icon 
        name='clear'
        color='#9E9E9E'
        onPress={() => this.deleteItem(item.MaHoSo)}
      /></View>
      <TouchableOpacity style={{padding: 15, alignSelf: 'stretch', paddingTop: -15}} onPress = {() => {this.props.navigation.navigate("DetailScreen", {data: item})}}>
        <View style= {{flexDirection: "row", justifyContent: 'space-between'}}><View style= {{}}><Text style={styles.ma}>{item.MaHoSo}</Text></View><View style= {{}}><Text style={styles.ngay}>{item.NgayTiepNhan}</Text></View></View>
        <View style= {{padding: 5}}>
        <Text style={styles.chu}>Chủ hồ sơ : {item.ChuHoSo}</Text>
        <Text style={styles.ten}>Tên hồ sơ : {item.TenHoSo}</Text>
        </View>
        <View style= {{alignSelf: 'flex-end'}}><Text style={styles.trangthai}>{item.TrangThaiMobile}  </Text></View>
      </TouchableOpacity>
      </View>
    )}

    showLoading = () => {
          return ( <View>
            <View style={styles.place}>
          <Placeholder.ImageContent
            size={60}
            animate="fade"
            lineNumber={6}
            lineSpacing={5}
            lastLineWidth="30%"
            onReady={!this.state.loading}
          >
          </Placeholder.ImageContent></View>
          <View style={styles.place}>
          <Placeholder.ImageContent
            size={60}
            animate="fade"
            lineNumber={6}
            lineSpacing={5}
            lastLineWidth="30%"
            onReady={!this.state.loading}
          >
          </Placeholder.ImageContent></View>
          <View style={styles.place}>
          <Placeholder.ImageContent
            size={60}
            animate="fade"
            lineNumber={6}
            lineSpacing={5}
            lastLineWidth="30%"
            onReady={!this.state.loading}
          >
          </Placeholder.ImageContent></View>
          <View style={styles.place}>
          <Placeholder.ImageContent
            size={60}
            animate="fade"
            lineNumber={6}
            lineSpacing={5}
            lastLineWidth="30%"
            onReady={!this.state.loading}
          >
          </Placeholder.ImageContent></View>
          <View style={styles.place}>
          <Placeholder.ImageContent
            size={60}
            animate="fade"
            lineNumber={6}
            lineSpacing={5}
            lastLineWidth="30%"
            onReady={!this.state.loading}
          >
          </Placeholder.ImageContent></View>
          </View>)
    }
  render() {
    if(this.state.loading === true) {
      return (
        <View>{this.showLoading()}</View>
      )
    }
    else{
    return (
        <View style={{flex: 1, backgroundColor: '#E0E0E0'}}>
          <FlatList
                data={this.state.data1}
                renderItem={this._renderItem}
                keyExtractor={(item, index) => index.toString()}
                //ListEmptyComponent={this.showEmptyListView()}
                extraData={this.state}
        />
        <ActionButton buttonColor="rgba(231,76,60,1)" bgColor="rgba(0,0,0,0.6)">
          <ActionButton.Item buttonColor='#9b59b6' title="Tìm kiếm hồ sơ" onPress={() => this.props.navigation.navigate("SearchScreen")}>
            <Icon name="search" color="#fff" size={20} />
          </ActionButton.Item>
          <ActionButton.Item buttonColor='#3498db' title="Thông báo" onPress={() => {}}>
            <Icon name="notifications" color="#fff" size={20} />
          </ActionButton.Item>
          <ActionButton.Item buttonColor='#1abc9c' title="Thông tin ứng dụng" onPress={() => {}}>
            <Icon name="info" color="#fff" size={20} />
          </ActionButton.Item>
        </ActionButton>
        </View>
    );
    }
  }
}
