import React, { Component } from "react";
import {Toolbar, Badge, IconToggle} from "react-native-material-ui"
import {View} from "react-native"

export default class Head extends Component {
  constructor(props) {
    super(props);

    this.state = {
        searchText: '',
    };
}

handleChange(timkiem){
  this.props.handleChangeShow(timkiem);
}

  render() {
    if(this.props.name){
      if( this.props.name === "Thông tin chi tiết hồ sơ"){
          return (
          <Toolbar
        style={{
        container: { backgroundColor: "#4caf50", height: 60},
        titleText: { fontWeight: 'normal', fontSize: 20 },
      }}
        key="toolbar"
        leftElement="arrow-back"
        onLeftElementPress={() => this.props.navigation.goBack()}
        centerElement={this.props.name}
            />
          );}
        else{
            return (
          <Toolbar
            style={{
            container: { backgroundColor: "#4caf50", height: 60,},
            titleText: { fontWeight: 'normal', fontSize: 20 },
          }}
          leftElement="arrow-back"
          onLeftElementPress={() => this.props.navigation.goBack()}
            key="toolbar"
            centerElement={this.props.name}
            />);
        }
        }
    else{
    return (
      <Toolbar
        style={{
        container: { backgroundColor: "#4caf50", height: 60},
        titleText: { fontWeight: 'normal', fontSize: 20 },
      }}
        key="toolbar"
        centerElement="Hồ sơ theo dõi"
        searchable={{
          autoFocus: true,
          placeholder: 'Nhập mã hồ sơ',
          onChangeText: (timkiem) => this.handleChange(timkiem),
          onSearchClosed: () => this.setState({ searchText: '' }),
          onSubmitEditing: (timkiem) => this.handleChange(timkiem)
        }}
            />
    );}
  }
}
