import React, { Component } from "react";
import { View, AsyncStorage} from "react-native";
import {Text, Button} from "react-native-elements";
import styles from "./Styles/MainScreenStyle"
import Head from "../Components/Header"
import MainBody from "../Components/MainBody";
import {StatusBar} from 'react-native';

export default class MainScreen extends Component {
  constructor(props) {
    super();
    this.state = {
        timkiem : '',
    };
  }
  changeShow(timkiem){
    this.setState({timkiem: timkiem});
  }
    
  render() {
    return (
        <View style={styles.container}>
        <StatusBar
        />
        <Head  {...this.props} handleChangeShow={this.changeShow.bind(this)}/>
        <MainBody  {...this.props} timkiem={this.state.timkiem}/>
        </View>
    );
  }
}
