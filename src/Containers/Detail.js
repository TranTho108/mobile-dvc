import React, { Component } from "react";
import { View, AsyncStorage} from "react-native";
import {Text, Button} from "react-native-elements";
import styles from "./Styles/MainScreenStyle"
import Head from "../Components/Header"
import DetailBody from "../Components/DetailBody";

export default class Detail extends Component {
    
  render() {
    return (
        <View style={styles.container}>
        <Head name="Thông tin chi tiết hồ sơ" {...this.props}/>
        <DetailBody {...this.props}/>
        </View>
    );
  }
}
