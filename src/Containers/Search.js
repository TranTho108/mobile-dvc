import React, { Component } from "react";
import { View, AsyncStorage} from "react-native";
import {Text, Button} from "react-native-elements";
import styles from "./Styles/MainScreenStyle"
import Head from "../Components/Header"
import SearchBody from "../Components/SearchBody"

export default class Search extends Component {
    
  render() {
    return (
        <View style={styles.container}>
        <Head name="Tra cứu thông tin hồ sơ" {...this.props}/>
        <SearchBody {...this.props}/>
        </View>
    );
  }
}
