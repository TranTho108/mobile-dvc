import React, { Component } from "react";
import { View, AsyncStorage} from "react-native";
import {Text, Button} from "react-native-elements";
import styles from "./Styles/MainScreenStyle"
import ScanBody from "../Components/ScanBody";

export default class Scan extends Component {
    
  render() {
    return (
        <View style={styles.container}>
        <ScanBody {...this.props}/>
        </View>
    );
  }
}
