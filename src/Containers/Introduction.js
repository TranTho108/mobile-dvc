import React from 'react';
import { View, AsyncStorage, FlatList, ImageBackground, ScrollView, Text, StyleSheet, Modal, Image,TouchableHighlight,Animated, StatusBar, TouchableOpacity, ActivityIndicator} from "react-native";
import { Button } from 'react-native-elements';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button'
import MainScreen from './MainScreen';

const arr =  [
              {
                "url": 'https://dvc1.mard.gov.vn',
                "name": "Bộ Nông nghiệp & Phát triển nông thôn",
              }
            ]
const arr1 =  [
              {
                "url": 'http://dichvucong.hatinh.gov.vn',
                "name": " Trung tâm HCC Tỉnh Hà Tĩnh",
              },
              {
                "url": 'http://dichvucong.hatinhcity.gov.vn',
                "name": " Trung tâm HCC Thành phố Hà Tĩnh & Các xã",
              },
              {
                "url": 'http://dvchuongson.hatinh.gov.vn',
                "name": " Trung tâm HCC Huyện Hương Sơn & Các xã",
              },
              {
                "url": 'http://dvcnghixuan.hatinh.gov.vn',
                "name": "Trung tâm HCC Huyện Nghi Xuân & Các xã",
              },
              {
                "url": 'http://dvcductho.hatinh.gov.vn',
                "name": "Trung tâm HCC Huyện Đức Thọ & Các xã",
              },
              {
                "url": 'http://dvchonglinh.hatinh.gov.vn',
                "name": "Trung tâm HCC Thị xã Hồng Lĩnh & Các xã",
              },
              {
                "url": 'http://dvchuongkhe.hatinh.gov.vn',
                "name": " Trung tâm HCC Huyện Hương Khê & Các xã",
              },
              {
                "url": 'http://dvcthachha.hatinh.gov.vn',
                "name": "Trung tâm HCC Huyện Thạch Hà & Các xã",
              },
              {
                "url": 'http://dvctxkyanh.hatinh.gov.vn',
                "name": " Trung tâm HCC Thị xã Kỳ Anh & Các xã",
              },
              {
                "url": 'http://dvclocha.hatinh.gov.vn',
                "name": "Trung tâm HCC Huyện Lộc Hà & Các xã",
              },
              {
                "url": 'http://dvcvuquang.hatinh.gov.vn',
                "name": "Trung tâm HCC Huyện Vũ Quang & Các xã",
              },
              {
                "url": 'http://dvccamxuyen.hatinh.gov.vn',
                "name": "Trung tâm HCC Huyện Cẩm Xuyên & Các xã",
              },
              {
                "url": 'http://dvckyanh.hatinh.gov.vn',
                "name": "Trung tâm HCC Huyện Kỳ Anh & Các xã",
              },
              {
                "url": 'http://dvccanloc.hatinh.gov.vn',
                "name": "Trung tâm HCC Huyện Can Lộc & Các xã",
              }
            ]
const arr2 =  [
              {
                "url": 'http://nshn.com.vn',
                "name": "Công ty TNHH Một thành viên Nước sạch Hà Nội",
              },
              {
                "url": 'http://dvc.nuocsachso3hn.vn',
                "name": "Công ty Cổ phần sản xuất Kinh doanh Nước sạch số 3 Hà Nội",
              }
            ]

class Introduction extends React.Component {
  constructor(props) {
    super(props);

    this.icons = {
      'up'    : require('../Images/Arrowhead-01-128.png'),
      'down'  : require('../Images/Arrowhead-Down-01-128.png')
  };

    this.state = {
      modalVisible: false,
      expanded    : false,
      animation   : new Animated.Value(),
      expanded1    : false,
      animation1   : new Animated.Value(),
      expanded2    : false,
      animation2   : new Animated.Value(),
      group1: null,
      group2: null,
      group3: null,
      arr: arr,
      arr1: arr1,
      arr2: arr2,
      isLoading: true
    };
  }

  async componentWillMount(){
    const once = await AsyncStorage.getItem('once');
    if(once !== null)
    {
      this.props.navigation.replace('MainScreen')
    }
    else{
      this.setState({isLoading: false})}
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  toggle(){
    let initialValue    = this.state.expanded? this.state.maxHeight + this.state.minHeight : this.state.minHeight,
        finalValue      = this.state.expanded? this.state.minHeight : this.state.maxHeight + this.state.minHeight;

    this.setState({
        expanded : !this.state.expanded
    });

    this.state.animation.setValue(initialValue);
    Animated.spring(
        this.state.animation,
        {
            toValue: finalValue
        }
    ).start();
}

_setMaxHeight(event) {
  if (!this.state.maxHeight) {
    this.setState({
      maxHeight: event.nativeEvent.layout.height,
    });
  }
}

_setMinHeight(event) {
  if (!this.state.minHeight) {
    this.setState({
      minHeight: event.nativeEvent.layout.height,
      animation: new Animated.Value(event.nativeEvent.layout.height),
    });
  }
}

toggle1(){
  let initialValue   = this.state.expanded1? this.state.maxHeight1 + this.state.minHeight1 : this.state.minHeight1,
      finalValue      = this.state.expanded1? this.state.minHeight1 : this.state.maxHeight1 + this.state.minHeight1;

  this.setState({
      expanded1 : !this.state.expanded1
  });

  this.state.animation1.setValue(initialValue);
  Animated.spring(
      this.state.animation1,
      {
          toValue: finalValue
      }
  ).start();
}

_setMaxHeight1(event) {
if (!this.state.maxHeight1) {
  this.setState({
    maxHeight1: event.nativeEvent.layout.height,
  });
}
}

_setMinHeight1(event) {
if (!this.state.minHeight1) {
  this.setState({
    minHeight1: event.nativeEvent.layout.height,
    animation1: new Animated.Value(event.nativeEvent.layout.height),
  });
}
}

toggle2(){
  let initialValue   = this.state.expanded2? this.state.maxHeight2 + this.state.minHeight2 : this.state.minHeight2,
      finalValue      = this.state.expanded2? this.state.minHeight2 : this.state.maxHeight2 + this.state.minHeight2;

  this.setState({
      expanded2 : !this.state.expanded2
  });

  this.state.animation2.setValue(initialValue);
  Animated.spring(
      this.state.animation2,
      {
          toValue: finalValue
      }
  ).start();
}

_setMaxHeight2(event) {
if (!this.state.maxHeight2) {
  this.setState({
    maxHeight2: event.nativeEvent.layout.height,
  });
}
}

_setMinHeight2(event) {
if (!this.state.minHeight2) {
  this.setState({
    minHeight2: event.nativeEvent.layout.height,
    animation2: new Animated.Value(event.nativeEvent.layout.height),
  });
}
}

_setUrl = async (url, name) => {
  await AsyncStorage.setItem('url', url);
  await AsyncStorage.setItem('name', name);
  await AsyncStorage.setItem('once','1');
}



  render() {
    let icon1 = this.icons['down'];
    let icon = this.icons['down'];
    let icon2 = this.icons['down'];

    if(this.state.expanded){
        icon = this.icons['up'];
    }

    if(this.state.expanded1){
      icon1 = this.icons['up'];
    }
    if(this.state.expanded2){
    icon2 = this.icons['up'];
    }

    const {arr, arr1, arr2} = this.state;
    if(this.state.isLoading == true){
      return (
        <View style={[styles.container2, styles.horizontal]}>
        </View>
      )
    }

    return (
      <ImageBackground resizeMode='cover' source={{uri :'http://wellworthsurgical.com/wp-content/uploads/parser/work-desk-background-1.jpg'}} style={{width: '100%', height: '100%'}}>
      <View style={{justifyContent: 'center',alignItems: 'center',flex: 1}}>
      <StatusBar />
      <Image
        style={{width: 200, height: 200, marginBottom: 200}}
        source={require('../Images/logo.png')}
      />
        <Button
            title='Chọn đơn vị thường sử dụng'
            buttonStyle={{
                backgroundColor: "#3F51B5",
                height: 45,
                borderColor: "transparent",
                borderWidth: 0,
                borderRadius: 10,
                alignSelf: 'center',
                width: 300,
                height: 50
              }}
              containerStyle={{ marginTop: 20 }}
              onPress={() => {
                this.setModalVisible(true);
              }}
        />
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
          }}
          >
      <View style={{flex: 1,
          flexDirection: 'column',
          backgroundColor: '#fff',
          marginTop: 100,
          borderTopLeftRadius: 10,
          borderTopRightRadius: 10}}>
        <View style={{marginTop: 10, marginBottom: 10}}><Text style={{textAlign: 'center', color: 'black', fontSize:18}}>Chọn Đơn Vị</Text></View>
        <ScrollView>
        <Animated.View 
            style={[styles.container,{height: this.state.animation}]}>
            <TouchableOpacity style={styles.titleContainer} onLayout={this._setMinHeight.bind(this)}
                    onPress={this.toggle.bind(this)}
                    underlayColor="#f1f1f1">
                    <Text style={styles.title1}>Cấp Bộ</Text>
                    <Image
                        style={styles.buttonImage}
                        source={icon}
                    ></Image>
                </TouchableOpacity>
            
            <View style={styles.body} onLayout={this._setMaxHeight.bind(this)}>
            <RadioGroup 
              size={24}
              thickness={2}
              color='black'
              highlightColor='#ccc8b9'
              selectedIndex={this.state.group1}
              onSelect = {(index, value) => {this.setState({group2: null,group1: index,group3: null}), this._setUrl(arr[0].url, arr[0].name)}}>

              <RadioButton value={'bnn'} >
                <Text> Bộ Nông nghiệp & Phát triển nông thôn</Text>
              </RadioButton>

            </RadioGroup>
            </View>

        </Animated.View>

        <Animated.View 
            style={[styles.container,{height: this.state.animation1}]}>
            <TouchableOpacity style={styles.titleContainer} onLayout={this._setMinHeight1.bind(this)}
                    onPress={this.toggle1.bind(this)}
                    underlayColor="#f1f1f1">
                    <Text style={styles.title1}>Tỉnh Hà Tĩnh</Text>
                    <Image
                        style={styles.buttonImage}
                        source={icon1}
                    ></Image>
                </TouchableOpacity>
            
            <View style={styles.body} onLayout={this._setMaxHeight1.bind(this)}>
              <RadioGroup 
              size={24}
              thickness={2}
              color='black'
              highlightColor='#ccc8b9'
              selectedIndex={this.state.group2}
              onSelect = {(index, value) => {this.setState({group1: null,group2: index,group3: null}), this._setUrl(arr1[index].url, arr1[index].name)}}>

              <RadioButton value={'ht1'} >
                <Text> Trung tâm HCC Tỉnh Hà Tĩnh</Text>
              </RadioButton>

              <RadioButton value={'ht2'}>
                <Text> Trung tâm HCC Thành phố Hà Tĩnh & Các xã</Text>
              </RadioButton>

              <RadioButton value={'ht3'}>
                <Text> Trung tâm HCC Huyện Hương Sơn & Các xã</Text>
              </RadioButton>

              <RadioButton value={'ht4'}>
                <Text> Trung tâm HCC Huyện Nghi Xuân & Các xã</Text>
              </RadioButton>

              <RadioButton value={'ht5'}>
                <Text> Trung tâm HCC Huyện Đức Thọ & Các xã</Text>
              </RadioButton>

              <RadioButton value={'ht6'}>
                <Text> Trung tâm HCC Thị xã Hồng Lĩnh & Các xã</Text>
              </RadioButton>

              <RadioButton value={'ht7'}>
                <Text> Trung tâm HCC Huyện Hương Khê & Các xã</Text>
              </RadioButton>

              <RadioButton value={'ht8'}>
                <Text> Trung tâm HCC Huyện Thạch Hà & Các xã</Text>
              </RadioButton>
              
              <RadioButton value={'ht9'}>
                <Text> Trung tâm HCC Thị xã Kỳ Anh & Các xã</Text>
              </RadioButton>

              <RadioButton value={'ht10'}>
                <Text> Trung tâm HCC Huyện Lộc Hà & Các xã</Text>
              </RadioButton>

              <RadioButton value={'ht11'}>
                <Text> Trung tâm HCC Huyện Vũ Quang & Các xã</Text>
              </RadioButton>

              <RadioButton value={'ht12'}>
                <Text> Trung tâm HCC Huyện Cẩm Xuyên & Các xã</Text>
              </RadioButton>

              <RadioButton value={'ht13'}>
                <Text> Trung tâm HCC Huyện Kỳ Anh & Các xã</Text>
              </RadioButton>

              <RadioButton value={'ht14'}>
                <Text> Trung tâm HCC Huyện Can Lộc & Các xã</Text>
              </RadioButton>

              </RadioGroup>
            </View>

        </Animated.View>

        <Animated.View 
            style={[styles.container,{height: this.state.animation2}]}>
                <TouchableOpacity 
                    style={styles.button} 
                    onPress={this.toggle2.bind(this)}
                    underlayColor="#f1f1f1"
                    style={styles.titleContainer} onLayout={this._setMinHeight2.bind(this)}>
                    <Text style={styles.title1}>Các đơn vị khác</Text>
                    <Image
                        style={styles.buttonImage}
                        source={icon2}
                    ></Image>
                </TouchableOpacity>
            
            <View style={styles.body} onLayout={this._setMaxHeight2.bind(this)}>
            <RadioGroup 
              size={24}
              thickness={2}
              color='black'
              highlightColor='#ccc8b9'
              selectedIndex={this.state.group3}
              onSelect = {(index, value) => {this.setState({group2: null,group3: index,group1: null}), this._setUrl(arr2[index].url, arr2[index].name)}}>

              <RadioButton value={'nshn'} >
                <Text> Công ty TNHH Một thành viên Nước sạch Hà Nội</Text>
              </RadioButton>

              <RadioButton value={'ns3hn'} >
                <Text> Công ty Cổ phần sản xuất Kinh doanh Nước sạch số 3 Hà Nội</Text>
              </RadioButton>

            </RadioGroup>
            </View>

        </Animated.View>
        </ScrollView>
        </View>
      <View style={{flexDirection: 'row', justifyContent: 'flex-end', backgroundColor: '#fff', paddingBottom: 10}}>
      <Button
                title='Thoát'
                buttonStyle={{
                backgroundColor: "#fff",
                height: 45,
                borderColor: "transparent",
                borderWidth: 0,
                borderRadius: 20,
                alignSelf: 'center'
              }}
              containerStyle={{ marginTop: 20 }}
              textStyle={{color: 'black'}}
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
              }}
              />
              <Button
                title='Tiếp tục'
                buttonStyle={{
                backgroundColor: "#fff",
                height: 45,
                borderColor: "transparent",
                borderWidth: 0,
                borderRadius: 20,
                alignSelf: 'center'
              }}
              containerStyle={{ marginTop: 20 }}
              textStyle={{color: 'black'}}
              onPress={() => {this.props.navigation.navigate('MainScreen') + this.setModalVisible(!this.state.modalVisible)}}
              />
              </View>
        </Modal>
      </View>
      </ImageBackground>
      );
  }
}

const styles = StyleSheet.create({
  container   : {
    backgroundColor: '#fff',
    margin:10,
    overflow:'hidden',
},
  title: {
    textAlign: 'center',
    fontSize: 22,
    fontWeight: '300',
    marginBottom: 20,
  },
  header: {
    backgroundColor: '#F5FCFF',
    padding: 10,
  },
  headerText: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: '500',
  },
  content: {
    padding: 20,
    backgroundColor: '#fff',
  },
  titleContainer : {
    flexDirection: 'row'
},
title1       : {
    flex    : 1,
    padding : 10,
    color   :'#2a2f43',
    fontWeight:'bold'
},
button      : {

},
buttonImage : {
    width   : 30,
    height  : 25
},
body        : {
    padding     : 10,
    paddingTop  : 0
},
container2: {
  flex: 1,
  justifyContent: 'center',
  backgroundColor: '#E0E0E0'
},
horizontal: {
  flexDirection: 'row',
  justifyContent: 'space-around',
  padding: 10
}
});

export default Introduction;