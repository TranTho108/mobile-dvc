import React from "react";
import { Dimensions, View, TouchableOpacity, Image } from 'react-native'
import { createStackNavigator, createBottomTabNavigator } from "react-navigation";
import { Icon, Text } from "react-native-elements"
import MainScreen from "../Containers/MainScreen"
import Introduction from "../Containers/Introduction"
import Search from "../Containers/Search"
import Scan from "../Containers/Scan"
import Detail from "../Containers/Detail"

const {height,width} = Dimensions.get('window');

const AppNavigator = createStackNavigator({
    MainScreen: { screen: MainScreen},
    Introduction: { screen: Introduction},
    SearchScreen: { screen: Search},
    ScanScreen: { screen: Scan},
    DetailScreen: { screen: Detail}
},
{
    headerMode: 'none',
    initialRouteName: "Introduction",
    transitionConfig: () => ({
        screenInterpolator: sceneProps => {
            const { layout, position, scene } = sceneProps;
            const { index } = scene;
    
            const translateX = position.interpolate({
                inputRange: [index - 1, index, index + 1],
                outputRange: [layout.initWidth, 0, 0]
            });
    
            const opacity = position.interpolate({
                inputRange: [
                    index - 1,
                    index - 0.99,
                    index,
                    index + 0.99,
                    index + 1
                ],
                outputRange: [0, 1, 1, 0.3, 0]
            });
    
            return { opacity, transform: [{ translateX }] };
        }
    })
}
);


export default AppNavigator;
