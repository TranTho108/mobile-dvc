import { AsyncStorage } from 'react-native';
import axios from "axios"
import {parseString} from "react-native-xml2js"

const SearchFileByCode = async(MaHoSo) => {
  const xmlReqBody = `<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">   <soap:Body>     <ServiceLayThongTinHoSo xmlns="http://schemas.microsoft.com/sharepoint/soap/">       <tukhoa>`+ MaHoSo +`</tukhoa>     </ServiceLayThongTinHoSo>   </soap:Body> </soap:Envelope>`
  var file = await axios('http://113.160.53.62:9009/_layouts/tandan/dvc/DVCService.asmx?wsdl', {
    method: 'POST',
    headers: {
      "Content-Type": 'text/xml; charset="utf-8"'
    },
    data: xmlReqBody,
    dataType: "xml",
  })
  .then(function (response) {
    var xml = response.data
    parseString(xml, function (err, result) {
      var t = result['soap:Envelope']
      var t1 = t['soap:Body']
      var t2 = t1[0].ServiceLayThongTinHoSoResponse[0].ServiceLayThongTinHoSoResult[0]
      xml = t2
    });
    return xml;
  })
  .then((data) => {
    var data1 = JSON.parse(data)
    data2 = data1.DSHoSo
    return data2
  }
)
  .catch((err) => console.log(err))
  return file;
  }

module.exports = SearchFileByCode;